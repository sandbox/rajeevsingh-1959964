Dynamic Menu Blocks generates a block that loads a menu based on a menu reference that exists in the active trail of another menu.

Installation
------------

Place this folder and all subfolders into your modules directory (e.g. 
sites/all/modules/dynamic_menu_blocks)

Enable the Dynamic Menu Blocks module from Drupal's module list (admin/modules in 
Drupal 7).



How to use Dynamic Menu Blocks
----------------------

Cross your fingers.



To Do
-----

- UI
- Multiple blocks
- Test